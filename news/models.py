from django.db import models


class Categoria(models.Model):
    nombre = models.CharField(max_length=144)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.nombre

class Noticia(models.Model):
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=144)
    contenido = models.TextField()
    imagen_news = models.ImageField(upload_to = 'pic/')
    autor = models.CharField(max_length=144)
    referencia = models.CharField(max_length=144)
    created_at = models.DateField()
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.titulo