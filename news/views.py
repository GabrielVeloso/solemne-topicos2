from django.shortcuts import render
from news.models import Noticia

def home(request):
    template_name = 'index.html'
    data = {} 
    data['noticias'] = Noticia.objects.all().order_by('created_at')[:6]
    
    return render(request, template_name, data)

