from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static # new
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('news/', include('news.urls')),
]

if settings.DEBUG: # new
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)